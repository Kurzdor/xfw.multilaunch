# SPDX-License-Identifier: MIT
# Copyright (c) 2020-2022 XVM Contributors 


#
# project
#
cmake_minimum_required (VERSION 3.23)
project(xfw_multilaunch)



#
# packages
#

set(PYBIND11_NOPYTHON ON)
find_package(Python2 COMPONENTS Development.Module REQUIRED)
find_package(pybind11 REQUIRED)



#
# library
#

add_library(xfw_multilaunch SHARED)

target_sources(xfw_multilaunch PRIVATE
    "pythonModule.cpp"
)

target_compile_definitions(xfw_multilaunch PRIVATE "_CRT_SECURE_NO_WARNINGS")


set_target_properties(xfw_multilaunch PROPERTIES CXX_STANDARD 20)
set_target_properties(xfw_multilaunch PROPERTIES CXX_STANDARD_REQUIRED ON)

target_link_libraries(xfw_multilaunch Python2::Module pybind11::pybind11)

set_target_properties(xfw_multilaunch PROPERTIES SUFFIX ".pyd")

install(TARGETS  xfw_multilaunch
        RUNTIME DESTINATION bin
        ARCHIVE DESTINATION lib
        LIBRARY DESTINATION lib)

install(
  FILES "$<$<OR:$<CONFIG:Debug>,$<CONFIG:RelWithDebInfo>>:$<TARGET_PDB_FILE:xfw_multilaunch>>"
  DESTINATION "bin"
)
